# Using Containers in Science

## About the Workshop

A container is a standard unit of software that packages up code and all its dependencies so the
application runs quickly and reliably from one computing environment to another. A container image
is a lightweight, standalone and executable package of software that includes everything to run
an application. Two popular container solutions are [Podman](https://podman-desktop.io/) and
[Apptainer](https://apptainer.org/) (formerly known as *Singularity*).

In this session you will learn how to use containers on your local system or on an HPC system. At
the end of the course you will also be able to package your own application into your custom
container. By uploading the container image to a registry, you learn how to share your work with
others and simplify the distribution and increase the portability of your software.

## Prerequisites

In this course we use Podman and Apptainer from the Unix Shell. Some previous experience with
the shell is expected.

### Podman installation

If you want to install Podman on your local system, please follow the installation guide for your
Operating System below.

#### Linux

Please refer to the
[Installation Guide for Podman Desktop for Linux](https://podman-desktop.io/docs/installation/linux-install)
for up-to-date installation instructions.
Make sure that you also install Podman itself according to the
[installation instructions](https://podman.io/docs/installation#installing-on-linux)
beforehand.

#### Mac OS

Please refer to the
[Installation Guide for MacOS](https://podman-desktop.io/docs/installation/macos-install)
for up-to-date installation instructions.
Make sure that you also install Podman itself according to the
[installation instructions](https://podman.io/docs/installation#macos)
beforehand.

#### Windows

Please refer to the
[Installation Guide for Windows](https://podman-desktop.io/docs/installation/windows-install)
for up-to-date installation instructions.
Podman itself is then already part of the installation.

#### Verify your Installation

If you successfully installed Podman on your system, you can test your installation by running the
following command on the command-line:

!!! note ""

    ~~~{: .bash .copy }
    podman run hello-world
    ~~~

    ~~~
    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    ~~~

### Apptainer/Singularity Installation

Apptainer/Singularity is currently available for Linux-based OS. Please read the
[Installation Instructions](https://apptainer.org/docs/admin/main/installation.html)
