---
title: "(4) Publishing Images"
teaching: 15
exercises: 0
questions:
- "How to publish images on the GitLab Container Registry?"
- "How to assign version tags to your images?"
- "How to delete images or tags?"
objectives:
- "Get to grips with pushing images."
- "Assign tags to your images."
- "Delete images or tags created."
keypoints:
- "Pushing images."
- "Tagging images."
- "Deleting images or tags."
---

# Publishing Images to a Container Registry

## Introduction into Publishing Images

In the previous episode we learned how to build our own images.
In this episode we are dealing with how to publish our images in a container registry like the GitLab container registry or Docker Hub.

For later reference, here is the respective [GitLab documentation](https://docs.gitlab.com/ee/user/packages/container_registry/) and [Docker documentation](https://docs.docker.com/docker-hub/).

This is how a GitLab Container Registry page looks like. It displays the most useful commands initially.

![GitLab Container Registry commands](../images/gitlab-container-registry-commands.png){: width="800px"}

## GitLab Container Registry

Publishing to a GitLab container registry involves five steps:

1. Create a GitLab user.
2. Create a GitLab repository.
3. Create a tag for your image.
4. Log in into the GitLab container registry with a personal access token.
5. Push your image to the container registry.

### Create a GitLab User

In order to publish an image to the Helmholtz Codebase GitLab Container Registry you need to [sign up](https://codebase.helmholtz.cloud/users/sign_up) for this service.

### Create a GitLab Repository

Once you signed up in the Helmholtz Codebase GitLab service you can create your own [GitLab repository](https://docs.gitlab.com/ee/user/project/repository/).
By default, the GitLab container registry is enabled for a new GitLab project.

### Pull Image to be Published

In the following we will demonstrate how to publish a small image [hello-world](https://hub.docker.com/_/hello-world)
into the Helmholtz Codebase GitLab Container Registry.
You need to pull the image in order to have it locally on your computer:

!!! note ""

    ~~~{: .bash .copy }
    podman pull hello-world:latest
    ~~~

!!! note "Output"

    ~~~
    Resolved "hello-world" as an alias (/etc/containers/registries.conf.d/000-shortnames.conf)
    Trying to pull docker.io/library/hello-world:latest...
    Getting image source signatures
    Copying blob 719385e32844 done
    Copying config 9c7a54a9a4 done
    Writing manifest to image destination
    Storing signatures
    9c7a54a9a43cca047013b82af109fe963fde787f63f9e016fdc3384500c2823d
    ~~~

### Create a Tag for your Image

Image tags are alias names for rather cryptic image IDs and specify the respective container registry to which the image will be published along with a version string of that image:

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                        TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world     latest      9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

!!! note ""

    ~~~bash
    podman image tag <image name>:<tag> <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<tag>
    podman image tag hello-world:latest hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:latest
    ~~~

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~bash
    REPOSITORY                                                     TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world                                  latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project       latest      9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

### Log in into the GitLab container registry with a personal access token

Communication with a GitLab container registry is only possible if you [log in](https://docs.docker.com/engine/reference/commandline/login/) to the GitLab container registry on the command line interface beforehand.

!!! note ""

    ~~~bash
    podman login hcr.helmholtz.cloud --username <GitLab username>
    ~~~

!!! note "Output"

    ~~~
    Password:
    Login Succeeded!
    ~~~

Attention: You need to [create a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with scope `api` or `read_registry` and `write_registry` and use that one instead of your password.

### Push your Image to the container registry

The final step is now to publish your image to the GitLab container registry of your repository:

!!! note ""

    ~~~bash
    podman push <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<tag>
    podman push hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:latest
    ~~~

!!! note "Output"

    ~~~
    Getting image source signatures
    Copying blob 01bb4fce3eb1 done
    Copying config 9c7a54a9a4 done
    Writing manifest to image destination
    Storing signatures
    ~~~

On the GitLab side you are now able to see the published images in the GitLab container registry:

![GitLab Container Registry list](../images/gitlab-container-registry-list.png){: width="800px"}

The detailed list of all tags in a GitLab container registry can also be obtained:

![GitLab Container Registry tags](../images/gitlab-container-registry-tags.png){: width="800px"}

!!! task "Task: Publishing on Docker Hub"

    Publishing Docker images to a container registry like Docker Hub involves five steps which are quite similar to those five steps when publishing to a GitLab container registry:
    
    1. Create a [Docker ID](https://hub.docker.com/signup).
    2. Create a [Docker repository](https://docs.docker.com/docker-hub/repos/).
    3. Create a tag for your Docker image.
    4. Log in into Docker Hub with an [access tokens](https://docs.docker.com/docker-hub/access-tokens/).
    5. Push your image to the container registry.

    Are you able to reproduce these steps given the five steps for publishing Docker images to a GitLab container registry?

## Understanding Image Tags

Images can have several tags, i.e. alias names for the respective image ID. So far we were labeling the newest image with `latest`.

It is good practice labeling images with semantic version strings `[major].[minor].[patch]` like `1.2.3` according to their release version.
See https://semver.org/ for a full specification how semantic versioning works.

Depending on the container registry used your images need to be tagged differently:

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                                                  TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world                               latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project    latest      9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

!!! note ""

    ~~~bash
    podman image tag <image name>:<tag> <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<semantic version tag>
    podman image tag hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:latest hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:0.1.0
    ~~~

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                                                  TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world                               latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project    latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project    0.1.0       9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

As you can see a tag may include the following information:

* URL of the container registry, defaults to `docker.io`.
* The username of the user of the container registry service.
* The name of the repository in the container registry.
* The tag specifying the version of the image to be added to the container registry, defaults to `latest`.

!!! note "Note"

    The important aspect to emphasize here is that tags may refer to the same image as indicated by the same image ID.

## Delete Images or Tags from GitLab Container Registry

!!! note "Note"

    The remote images are irreversibly gone after deletion if they are not also stored locally.

Log in into GitLab, navigate to the tag you would like to delete and click the trash icon.

![GitLab Container Registry delete](../images/gitlab-container-registry-delete.png){: width="800px"}

## Remove Images or Tags Locally

!!! note "Note"

    The local images are irreversibly gone after deletion if they were not pushed to a remote registry earlier.

Tags can be deleted by their tag name:

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                                                             TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world                               latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project    latest      9c7a54a9a43c  4 months ago   19.9 kB
    hcr.helmholtz.cloud/hueser93/my-helloworld-image-project    0.1.0       9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

!!! note ""

    ~~~bash
    podman image rm <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<tag>
    podman image rm hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:0.1.0
    ~~~

!!! note "Output"

    ~~~
    Untagged: hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:0.1.0
    ~~~

!!! note ""

    ~~~bash
    podman image rm hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:latest
    ~~~

!!! note "Output"

    ~~~
    Untagged: hcr.helmholtz.cloud/hueser93/my-helloworld-image-project:latest
    ~~~

!!! note ""

    ~~~
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                        TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world     latest      9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

Images can be deleted by their image ID:

!!! note ""

    ~~~{: .bash .copy }
    podman image list
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                        TAG         IMAGE ID      CREATED        SIZE
    docker.io/library/hello-world     latest      9c7a54a9a43c  4 months ago   19.9 kB
    ~~~

!!! note ""

    ~~~bash
    podman image rm <image ID>
    podman image rm 9c7a54a9a43c
    ~~~

!!! note "Output"

    ~~~
    Untagged: docker.io/library/hello-world:latest
    Deleted: 9c7a54a9a43cca047013b82af109fe963fde787f63f9e016fdc3384500c2823d
    ~~~

Also, all unused images can be deleted with the `prune` sub-command:

!!! note ""

    ~~~{: .bash .copy }
    podman image prune
    ~~~
