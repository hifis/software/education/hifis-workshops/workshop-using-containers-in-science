---
title: (1) Introduction
teaching: 15
exercises: 0
questions:
- "What are containers, what is the difference to a VM and why should I use them?"
objectives:
- "Understand the basic concepts of containers."
- "Understand the difference between containers and a Virtual Machine (VM)."
keypoints:
- "A container is a set of processes that are isolated from the rest of the system."
- "A container provides all files necessary to support the processes"
- "The one big difference between containers and VMs is that containers share the host system's
  kernel with other containers."
- "Containers simplify the way how to ship, build and run applications."
---

# Introduction

<center>
![Portability Comic](../images/comic.png){width=500px}
</center>

## What are containers?

When we are talking about containers in this lesson, we do not think about the metal shipping
containers. But the concept, that heavily simplified and standardized shipping, can also be used
in today's operating systems. Imagine the ship as a system and the containers as a software
package. Each container is a completely independent, separate unit that can be reused on every
ship. Tools like Podman or Apptainer help with packaging, shipping and running of applications.

All of that sounds very similar to a solution, that you perhaps are already aware about? Yes,
Virtual Machines (VMs) have very similar goals: Isolating an application and all its dependencies to
convert them into a self-contained unit that could run anywhere. But what is the difference
between them?

## Difference between Virtualization and Containerization

A VM is simply a virtual computer. It executes each and every program like a real computer. It
runs on top of a physical machine with the help of a hypervisor. In virtualization a hypervisor
creates and runs virtual machines on top of a Host Operating System (OS) or sometimes directly on
bare-metal. The guest OS is itself a completely independent OS, including its own kernel. The
Instruction Set Architecture (ISA) is translated into the Host ISA by using techniques like
Hardware Virtualization or Binary Translation.

A container, on the other hand, is a light-weight OS running inside the host system. Instructions
run natively on the CPU, eliminating the need for instruction level emulation or just in time
compilation. The one big difference between containers and VMs is that containers share the host
system's kernel with other containers.

Both techniques have the same goal: isolate an application and its dependencies into a
self-contained unit that can run anywhere. Both techniques allow for a more efficient use of
computing resources.

![Virtualization vs. Containers](../images/vm-vs-containers.svg)

For a quick comparison between Virtual Machines and Containers have a look at the table below:

|               | Virtual Machines                                                                                              | Container                                                               |
|---------------|---------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------|
| Guest OS      | Each VM runs on virtual hardware and the kernel is loaded into its own memory region.                         | All containers share the same kernel.                                   |
| Communication | Through Ethernet Devices.                                                                                     | Standard IPC mechanisms like Signals, pipes, sockets, etc.              |
| Performance   | Small overhead as the Machine instructions need to be translated from Guest to Host OS.                       | Near native performance as compared to the underlying Host OS.          |
| Startup time  | Takes up to a few minutes to boot up.                                                                         | Containers can be booted up in a few seconds.                           |
| Isolation     | Sharing libraries, files, etc. between guests and between guests and host not natively possible.              | Subdirectories can be transparently mounted and can be shared.          |
| Storage       | VMs usually require more storage as the whole OS kernel and associated programs have to be installed and run. | Containers consume lower amount of storage since the Host OS is shared. |
| Security      | Fully isolated and hence more secure.                                                                         | Process-level isolation, possibly less secure.                          |

## Pros of Using Containers

- **Simplified application deployment** since containers are self-contained.
- As long as the correct kernel is in place **containers are easily portable and reusable**.
- **Testing and bug tracking can become less complicated**, since there is no difference between
  running the application locally, on a test server or in production.

## Cons of Using Containers

- **Less portability compared to VMs.** Since containers use the underlying kernel of the Host OS,
  they are not easily portable from e.g. Linux to Windows or MacOS without some additional level
  of virtualization inbetween.
- **Containers can be less secure.** They share the Host OS kernel. More potential for potential
  flaws and attacks do carry down into the underlying OS compared to VMs.
- **Containers can require more work upfront.** If you are using containers as intended your
  application is decomposed into its various constituent services.

