---
title: "(2) Pull and Run Containers"
teaching: 20
exercises: 15
questions:
- "What is Podman?"
- "How do I fetch and run images?"
- "How can I use folders from my local filesystem inside a container?"
objectives:
- "Pull and run images from public registries."
- "Run commands inside a container."
- "Clean up disk space by removing leftover containers."
- "Mount folders from the local file system into the container."
keypoints:
- "Use `podman pull` to fetch images from a registry and to save them locally."
- "`podman run` creates a container from an image and runs a command inside."
- "Use the option `-v` with the `podman run` command to mount a local folder into the container."
- "To cleanup leftover containers use the command `podman rm`."
---

# Pull and Run Containers

## Brief Introduction into Podman

At the time of writing Docker is the most popular and most widely used container solution on the
market. Due to licensing restrictions the use of Docker is often forbidden in a larger context if
used without a proper license.
In the following we are using Podman, an open-source alternative, instead of Docker, but most of
the commands can be used with Podman and Docker alike.
Please refer to the [Setup section for Podman](../../index.html) for installation instructions for your OS.

!!! note "Verify the Podman installation"

    !!! note ""
    
        ~~~{: .bash .copy }
        podman run hello-world
        ~~~
    
    !!! note "Output"

        ~~~
        Hello from Docker!
        This message shows that your installation appears to be working correctly.
        ~~~

## Basic Podman Commands

Now that everything is set up, it is time to issue the first `podman` commands.
Let's pull our first image from [Dockerhub](https://hub.docker.com/).
As a start we want to pull this [Python image](https://hub.docker.com/_/python).

!!! note ""

    ~~~{: .bash .copy }
    podman pull python
    ~~~

!!! note "Output"

    ~~~
    Resolved "python" as an alias (/etc/containers/registries.conf.d/000-shortnames.conf)
    Trying to pull docker.io/library/python:latest...
    Getting image source signatures
    Copying blob 9c94b131279a done
    Copying blob c485c4ba3831 done
    Copying blob d31b0195ec5f done
    Copying blob 9b1fd34c30b7 done
    Copying blob 4bc8eb4a36a3 done
    Copying blob de4cac68b616 done
    Copying blob 470924304c24 done
    Copying blob 8999ec22cbc0 done
    Copying config 28d8ca9ad9 done
    Writing manifest to image destination
    Storing signatures
    28d8ca9ad96db542a2071ad13fa82533a14441b84d52e91ad395d663deb2ec82
    ~~~

The `podman pull` command fetches the `python` image from a registry and saves it locally
to our system.
Use the command `podman images` to see a list of all images on your system.

!!! note ""

    ~~~{: .bash .copy }
    podman images
    ~~~

!!! note "Output"

    ~~~
    REPOSITORY                   TAG         IMAGE ID          CREATED          SIZE
    docker.io/library/python     latest      28d8ca9ad96d      11 days ago      1.03 GB
    ~~~

Let's now run a container based on the python image.
To do that we use the command `podman run`:

!!! note ""

    ~~~{: .bash .copy }
    podman run python
    ~~~

Nothing really seemed to happen this time.
This is not a bug.
Many things were going on behind the scenes:
When you call the command `podman run`, the podman client finds the image, loads up the container and runs a command inside the container.
We ran `podman run python` without providing a command, and thus it directly exited again without producing output.
Let's try again by providing a command to `podman run`.

!!! note ""

    ~~~{: .bash .copy }
    podman run python python3 --version
    ~~~

!!! note "Output"

    ~~~
    Python 3.13.2
    ~~~

We now ran the command `python3 --version` in the container and, as expected, the python version number was printed out to the terminal.
Again, once the command finished the container exits.
The general pattern for running commands in a container is
`podman run [options] image-name [command] [arguments]`.

Hopefully, you noticed that all of this happened pretty quickly.
Imagine you needed to boot up a virtual machine, run the command and destroy the VM.
That's the speed difference mentioned in the [introduction](../01-introduction).
The `podman ps` command shows you all containers that are currently running.

!!! note ""

    ~~~{: .bash .copy }
    podman ps
    ~~~

!!! note "Output"

    ~~~
    CONTAINER ID      IMAGE      COMMAND      CREATED        STATUS         PORTS        NAMES
    ~~~

As expected, since no containers are running, we see a blank line.
Use `podman ps -a` to get a more complete output:

!!! note ""

    ~~~{: .bash .copy }
    podman ps -a
    ~~~

!!! note "Output"

    ~~~
    CONTAINER ID        IMAGE                                 COMMAND              CREATED             STATUS                      PORTS               NAMES
    bf509388514d        docker.io/library/python:latest       "python3 --version"  7 seconds ago       Exited (0) 6 seconds ago                        hungry_blackwell
    71e6eac3b31c        docker.io/library/python:latest       "python3"            12 seconds ago      Exited (0) 11 seconds ago                       flamboyant_hodgkin
    ~~~

You might be wondering now if there is a way to run more than one command in a container.
Invoking the `run` command with the `-it` flag will give you an interactive _tty_ session in the container.
Then you can run as many commands in the container as you want to, like in a normal `bash` or Python interpreter.

!!! note ""

    ~~~{: .bash .copy }
    podman run -it python python3
    ~~~

!!! note "Output"

    ~~~
    Python 3.13.2 (main, Feb 25 2025, 05:25:21) [GCC 12.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>>
    ~~~

!!! note ""

    ~~~{: .bash .copy }
    podman run -it python bash
    ~~~

!!! note "Output"

    ~~~
    root@43f5d561dcd3:/#
    ~~~

Interestingly, we also get an interactive Python interpreter by only running the command `podman run -it python`.
Keep that in mind, we will see later why this works in that case.

So far, we have had no access to files from the local filesystem inside the container.
But you probably want to have exactly that.
With Podman, you can easily mount folders inside a container.
Supply the `-v` option when running the `podman run` command and as an argument pass the absolute path on the local filesystem on the left side and on the right side the absolute path inside the container.
Split both paths with a `:`.
You can use `-v` multiple times in one `podman run` command.
The command below mounts the directory `test` from the users `home`-directory to the path `/opt/test` inside the container.

!!! note ""

    ~~~{: .bash .copy }
    podman run -v $HOME/test:/opt/test --rm -it alpine:latest sh
    ~~~

!!! note "Output"

    ~~~
    Resolved "alpine" as an alias (/etc/containers/registries.conf.d/000-shortnames.conf)
    Trying to pull docker.io/library/alpine:latest...
    Getting image source signatures
    Copying blob 7264a8db6415 done
    Copying config 7e01a0d0a1 done
    Writing manifest to image destination
    Storing signatures
    / #
    ~~~

!!! note ""

    ~~~bash
    / # cd /opt/test
    / # touch foo
    / # exit
    $ cd $HOME/test
    $ ls
    ~~~

!!! note "Output"

    ~~~
    foo
    ~~~

!!! info "Package Permissions within the Container"

    Because of permissions and user IDs, it might happen, that you cannot access the files, that were written by the application within Podman.
    Podman uses the `root`-user if not specified differently somewhere else, which means, for example, that files, directories, and packages that were written or installed within the container would have user and group `root` as the default.
    This may result in an `access denied` error message if you try to access those artifacts with a non-root user.
    To avoid that, you have to pass you local user ID to Podman:
    
    ~~~
    -u $(id -u ${USER}):$(id -g ${USER})
    ~~~
    
    ~~~{: .bash .copy }
    podman run --rm -it \
           -u $(id -u ${USER}):$(id -g ${USER}) \
           -v $HOME/test:/opt/test \
           alpine:latest \
           sh
    ~~~

What about the `--rm` option though?
You might have already noticed that we can still see leftover containers from previous runs that even exited by running `podman ps -a`.
Throughout this lesson you will run `podman run` multiple times.
This will leave containers which in turn will occupy disk space.
It is necessary to regularly delete old containers from the disk.
To do that you can run the `podman rm` command.
Copy the container IDs from the output of `podman ps -a` and paste them alongside the command.

!!! note ""

    ~~~{: .bash .copy }
    podman rm bf509388514d 71e6eac3b31c
    ~~~

!!! note "Output"

    ~~~
    bf509388514d0d84f2def765a75677dcfd1624d0acf63756319423c087597e74
    71e6eac3b31c37ff603e8eb7af4064db1cc0a95afde07d24e6fc0203399d6667
    ~~~

This can be a repetitive task.
You can delete a bunch of containers in one go as shown in the following command:

!!! note ""

    ~~~{: .bash .copy }
    podman container prune
    ~~~

This command deletes all containers that are not running.

One last useful thing: Combine the `podman run` command with the option `--rm` as was shown before.
This will automatically delete the container once it exits from it.
But be careful, this is only useful, if you want to run the container only once.
It is definitely gone afterwards and will have to be re-created if you wish to run it again.

### Volumes

We have already seen how to mount folders from the host to the container (these are called _bind mounts_).
Volumes are the only way to have persistent data within a container and the reason why so many containers (like databases) use them.
Because often it is not needed to access the files from the host system, we can let Podman manage this persistent storage.

!!! note ""

    ~~~{: .bash .copy }
    podman volume ls
    ~~~

With this command we can list all existing volumes.

!!! note ""

    ~~~{: .bash .copy }
    podman volume create test_volume
    ~~~

Here we created a volume called `test_volume`.

!!! note ""

    ~~~{: .bash .copy }
    podman volume ls
    ~~~

!!! note "Output"

    ~~~
    DRIVER    VOLUME NAME
    local     test_volume
    ~~~

Volumes are particularly useful because they are OS independent and fully managed by Podman.
Some additional advantages of volumes:

* Volumes are easier to back up or migrate than bind mounts.
* Volumes can be more safely shared among multiple containers.

In the end, volumes are names for folders on the host (like `/` or `~`).
We can see their location on the host, if we inspect a volume.

!!! note ""

    ~~~{: .bash .copy }
    podman volume inspect test_volume
    ~~~

!!! note "Output"

    ~~~
    [
        {
            "Name": "test_volume",
            "Driver": "local",
            "Mountpoint": "/home/christianhueser/.local/share/containers/storage/volumes/test_volume/_data",
            "CreatedAt": "2024-06-06T10:52:21.049925299+02:00",
            "Labels": {},
            "Scope": "local",
            "Options": {}
        }
    ]
    ~~~

We can mount a volume like a folder from a host system by using its name on the left side of the `:` in `-v` argument.

!!! note ""

    ~~~{: .bash .copy }
    podman run -it \
           -v test_volume:/test \
           alpine:latest \
           touch /test/foo
    ~~~

After a volume is unmounted, we can use the following command to remove it.

!!! note ""
    
    ~~~{: .bash .copy }
    podman volume rm test_volume
    ~~~

    **Note:** All containers associated with a volume need to be removed beforehand.

## Task: Run your own image

!!! task "Task Description"

    Your goal in this exercise is to run a Jupyter notebook inside a container. You need to
    be able to open the Jupyter notebook in the browser of the host system and run Python commands
    inside. Delete the created container afterwards and make sure, that there are no leftover
    containers.
    
    1. Find the right image called `docker.io/jupyterhub/singleuser` (~300 MB) from Dockerhub and pull it.
    2. Create a folder called `jupyter` on your local system.
    3. Run a Jupyter container based on the image you just pulled. Bind the port `8888` to
       `localhost`. Mount the folder created locally into the container. Note the hints below.
    4. Open the Jupyter notebook in the browser of your host system by opening the link that
       appears in the terminal: `http://127.0.0.1:8888/lab?token=...`
    5. Run a simple calculation.
    6. Stop the container and delete the container image.
    
    !!! note ""

        **Hints:** Use the option `-p 127.0.0.1:8888:8888` to bind the Jupyter notebook port to your host
        system, while the option `-v $(pwd):/home/jovyan` mounts the current folder into the container.
        Please also read the section about
        [user-related configurations](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html#user-related-configurations)
        and add the following options: `--user root -e NB_USER="jovyan" -e CHOWN_HOME=yes -w "/home/jovyan"`

??? tip "Solution"

    We use the image [jupyterhub/singleuser](https://hub.docker.com/r/jupyterhub/singleuser) from Dockerhub. Start the container, using the `podman run` command.

    !!! note ""

        ~~~{: .bash .copy }
        podman run --name jupyter \
            -p 127.0.0.1:8888:8888 \
            -v $(pwd):/home/jovyan \
            --user root \
            -e NB_USER="jovyan" \
            -e CHOWN_HOME=yes \
            -w "/home/jovyan" \
            docker.io/jupyterhub/singleuser:latest
        ~~~
    
    ??? note "Output"
        
        ~~~ 
        Trying to pull docker.io/jupyterhub/singleuser:latest...
        Getting image source signatures
        Copying blob a1dc97bc6bde done  
        Copying blob 4f4fb700ef54 done  
        Copying blob 4a023cab5400 done  
        Copying blob db482cd896b1 done  
        Copying blob 2d3dcd388b50 done  
        Copying blob 4f4fb700ef54 skipped: already exists  
        Copying blob f36707047f95 done  
        Copying blob a8b990912fcc done  
        Copying blob 17b47c009bb1 done  
        Copying blob 4f4fb700ef54 skipped: already exists  
        Copying blob fcf155c2aa1c done  
        Copying blob a5edf3098f0b done  
        Copying blob d5909f1f0a26 done  
        Copying blob 4f4fb700ef54 skipped: already exists  
        Copying blob 303f36bf242a done  
        Copying blob 86a14a2a6897 done  
        Copying blob 046f8b883bf8 done  
        Copying blob 6f94921b1fc6 done  
        Copying blob 454859ecbe33 done  
        Copying blob f9abe245fcf5 done  
        Copying blob 4f4fb700ef54 skipped: already exists  
        Copying blob 5f7d42f2a47e done  
        Copying blob f2265e2338ff done  
        Copying config 0184d8a726 done  
        Writing manifest to image destination
        Storing signatures
        Entered start.sh with args: start-notebook.py
        Running hooks in: /usr/local/bin/start-notebook.d as uid: 0 gid: 0
        Done running hooks in: /usr/local/bin/start-notebook.d
        Ensuring /home/jovyan is owned by 1000:100 
        Running hooks in: /usr/local/bin/before-notebook.d as uid: 0 gid: 0
        Sourcing shell script: /usr/local/bin/before-notebook.d/10activate-conda-env.sh
        Done running hooks in: /usr/local/bin/before-notebook.d
        Running as jovyan: start-notebook.py
        [I 2024-06-06 08:56:11.414 ServerApp] jupyter_lsp | extension was successfully linked.
        [I 2024-06-06 08:56:11.417 ServerApp] jupyter_server_terminals | extension was successfully linked.
        [I 2024-06-06 08:56:11.421 ServerApp] jupyterlab | extension was successfully linked.
        [I 2024-06-06 08:56:11.423 ServerApp] nbclassic | extension was successfully linked.
        [I 2024-06-06 08:56:11.426 ServerApp] notebook | extension was successfully linked.
        [I 2024-06-06 08:56:11.427 ServerApp] Writing Jupyter server cookie secret to /home/jovyan/.local/share/jupyter/runtime/jupyter_cookie_secret
        [I 2024-06-06 08:56:11.648 ServerApp] notebook_shim | extension was successfully linked.
        [I 2024-06-06 08:56:11.760 ServerApp] notebook_shim | extension was successfully loaded.
        [I 2024-06-06 08:56:11.766 ServerApp] jupyter_lsp | extension was successfully loaded.
        [I 2024-06-06 08:56:11.769 ServerApp] jupyter_server_terminals | extension was successfully loaded.
        [I 2024-06-06 08:56:11.774 LabApp] JupyterLab extension loaded from /opt/conda/lib/python3.11/site-packages/jupyterlab
        [I 2024-06-06 08:56:11.774 LabApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
        [I 2024-06-06 08:56:11.775 LabApp] Extension Manager is 'pypi'.
        [I 2024-06-06 08:56:11.791 ServerApp] jupyterlab | extension was successfully loaded.
        [I 2024-06-06 08:56:11.795 ServerApp] nbclassic | extension was successfully loaded.
        [I 2024-06-06 08:56:11.797 ServerApp] notebook | extension was successfully loaded.
        [I 2024-06-06 08:56:11.797 ServerApp] Serving notebooks from local directory: /home/jovyan
        [I 2024-06-06 08:56:11.797 ServerApp] Jupyter Server 2.14.0 is running at:
        [I 2024-06-06 08:56:11.797 ServerApp] http://4dd7ecf0f6fc:8888/lab?token=07fb25cee707127c8de5d36290d2446d6c67b82aa2016e19
        [I 2024-06-06 08:56:11.797 ServerApp]     http://127.0.0.1:8888/lab?token=07fb25cee707127c8de5d36290d2446d6c67b82aa2016e19
        [I 2024-06-06 08:56:11.797 ServerApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
        [C 2024-06-06 08:56:11.800 ServerApp] 
            
            To access the server, open this file in a browser:
                file:///home/jovyan/.local/share/jupyter/runtime/jpserver-21-open.html
            Or copy and paste one of these URLs:
                http://4dd7ecf0f6fc:8888/lab?token=07fb25cee707127c8de5d36290d2446d6c67b82aa2016e19
                http://127.0.0.1:8888/lab?token=07fb25cee707127c8de5d36290d2446d6c67b82aa2016e19
        [I 2024-06-06 08:56:12.148 ServerApp] Skipped non-installed server(s): bash-language-server, dockerfile-language-server-nodejs, javascript-typescript-langserver, jedi-language-server, julia-language-server, pyright, python-language-server, python-lsp-server, r-languageserver, sql-language-server, texlab, typescript-language-server, unified-language-server, vscode-css-languageserver-bin, vscode-html-languageserver-bin, vscode-json-languageserver-bin, yaml-language-server
        [I 2024-06-06 08:56:20.602 LabApp] Build is up to date
        ~~~

    When your container is running you can open your browser at `http://127.0.0.1:8888/lab?token=...`. 
    After you closed the session you can remove it, using `podman rm [name]`.
    
    !!! note ""

        ~~~{: .bash .copy }
        podman rm jupyter
        ~~~

    !!! note "Output"
    
        ~~~
        jupyter
        ~~~

    !!! note ""
    
        ~~~{: .bash .copy }
        podman ps -a
        ~~~

    !!! note "Output"

        ~~~
        CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
        ~~~
