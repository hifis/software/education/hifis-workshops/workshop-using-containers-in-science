---
title: "(7) Pitfalls and Best Practices"
teaching: 20
exercises: 0
questions:
- ""
objectives:
- "Raise awareness for the most common mistakes made when working with Podman."
- "Give an overview of the considerations to be made when working with Podman."
keypoints:
- ""
---

# Pitfalls and Best Practices

## Most Important Point
* **Do not put sensitive information like keys or tokens into your VCS
  repositories or images** Use environment variables for these purposes
  instead
  * Command `run` has several
    [options for environment variables](https://docs.podman.io/en/latest/markdown/podman-run.1.html#env-file-file).
    The option `env-file` is considered to be one way to make sensitive
    information available in your containers via a confidential file.
  * Similarly, the Compose file or command may contain
    [keys or options for environment variables](https://docs.docker.com/compose/environment-variables/set-environment-variables/#substitute-with---env-file).
    The key `env_file` or option `env-file` are considered to be ways to
    provide sensitive information to your containers.

## Choosing a Good Starting Point
Carefully picked base images for custom containers can help to avoid many
security and maintenance issues.

* Choose images from trusted repositories and prefer official images from
  trusted publishers.
  This reduces the chance of an image being intentionally created to spread
  malicious software.
  <center>
  ![Filter Options](../images/filter-repositories.png){ align=center }
  </center>

* Pay attention to the maintenance status of the images you select.
  Images that have not been updated for a long time can carry security risks
  and are likely to cause compatibility issues.

* Use the smallest base image possible.
  For example, `alpine` Docker images which are based on `Alpine Linux`
  (and therefore also on `BusyBox`, a suite of Unix utilities shipped in a
  single executable, and `musl libc` as its C standard library, which is only
  partially binary compatible with `glibc`) are considered to be smaller that
  images based on most other Linux distributions.
  Furthermore, `slim` images  are images optimized in size with minimal
  packages installed that are needed to run the container.

## Create Minimal and Well-Focussed Containers
Each container is supposed to run one application only.
This makes it easier to maintain and manage them and reduces the time and
resources required to generate, start and run them.
* Make your images as small as possible
  * Only install the packages that are absolutely necessary
  * The image size can be further kept low by clearing caches (e.g. of the package manager)
* More complex applications can be decoupled by splitting them over several containers and connecting them via networks
  * **Caution!** This is an advanced topic with its own pitfalls.

## Leverage the Benefits of Automation
Containers can automatically be created with CI/CD toolchains (e.g. from GitLab, GitHub, Jenkins…) this can serve to reduce the amount of work involved while maintaining and updating your containers.
Especially in combination with version control and automated backing up and pushing to registries, these solutions help to reduce the amount of tedious labour involved with creating containers to a minimum.

## Cleaning up
When working with containers there may always be some files or processes being
leftover or residual occupying memory and storage.
Cleaning up can help reduce the workload on your system and free up disk space.
* Prune your images (`podman image prune` or even `podman image prune -a`)
  and containers (`podman container prune`) from time to time to free up
  storage on your system
* Use the option `rm` of the `run`-command to remove stopped containers
  automatically
* Clean up the used volumes regularly

## Further Reading

* [Keeping containers small](https://cloud.google.com/solutions/best-practices-for-building-containers#reduce_the_amount_of_clutter_in_your_image)
* [Image-building best practices](https://docs.docker.com/get-started/09_image_best/)
* [Using multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/)
* [Using labels to add metadata](https://docs.docker.com/config/labels-custom-metadata/)
* [Linting your Dockerfiles](https://hadolint.github.io/hadolint/)
