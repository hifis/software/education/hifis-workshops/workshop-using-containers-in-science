# Using Containers in Science

## Usage

To run the build and preview process run:

```bash
poetry install
poetry run mkdocs serve
```

## Maintainer(s)

Current maintainers of this lesson are 

* Tobias Huste (HZDR)
* Norman Ziegner (HZDR)
* Christian Hüser (HZDR)

## Authors

A list of contributors to the lesson can be found in [AUTHORS](AUTHORS)
